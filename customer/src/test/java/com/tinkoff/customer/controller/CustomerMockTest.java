package com.tinkoff.customer.controller;

import com.tinkoff.customer.dao.Customer;
import com.tinkoff.customer.dao.CustomerRepository;
import com.tinkoff.customer.services.CustomerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;

import static org.mockito.BDDMockito.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerMockTest {

    @Autowired
    private CustomerService customerService;

    @MockBean
    private CustomerRepository customerRepository;

    @Test
    public void mockTest() {
        given(customerRepository.findAll()).willReturn(
                List.of(
                        new Customer(1, "Alexandr", "Petrov", "alex@gmail.com", "1234567"),
                        new Customer(1, "Maxim", "Ivanov", "max@gmail.com", "1234567"),
                        new Customer(1, "Kirill", "Alexeev", "kirill@gmail.com", "1234567")
                ));

        List<Customer> customerList = customerService.getAll();
        customerList.forEach(System.out::println);
        verify(customerRepository, times(1)).findAll();
    }
}