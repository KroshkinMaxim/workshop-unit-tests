package com.tinkoff.customer.controller;

import com.tinkoff.customer.dao.Customer;
import com.tinkoff.customer.dao.CustomerRepository;
import com.tinkoff.customer.model.FraudResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.tinkoff.customer.utils.TestDataGenerators.getRandomDigits;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerTest {

    @Autowired
    private CustomerController customerController;

    @Autowired
    private CustomerRepository customerRepository;

    @MockBean
    private RestTemplate restTemplate;

    private Customer testCustomer;

    @BeforeEach
    public void init() {
        String serNo = "1234567";
        testCustomer = Customer.builder()
                .firstName("Alexander")
                .lastName("Petrov")
                .email("alex@gmail.com")
                .serNo(serNo)
                .build();
        testCustomer = customerRepository.save(testCustomer);
    }

    @AfterEach
    public void clear() {
        Optional<Customer> optionalCustomer = customerRepository.findById(testCustomer.getId());
        if (optionalCustomer.isPresent()) {
            customerRepository.deleteById(testCustomer.getId());
        }
    }

    @Test
    public void createCustomer() {
        String serNo = "1234567";

        Customer customer = Customer.builder()
                .firstName("Alexander")
                .lastName("Petrov")
                .email("alex@gmail.com")
                .serNo(serNo)
                .build();

        given(restTemplate.getForObject(
                anyString(),
                eq(FraudResponse.class),
                any(),
                eq(customer.getSerNo())
        )).willReturn(new FraudResponse(true));

        Customer serviceResponse = customerController.registerCustomer(customer);
        Customer repositoryResponse = customerRepository.findById(serviceResponse.getId()).orElse(null);
        assertNotNull(repositoryResponse);
        assertEquals(serNo, repositoryResponse.getSerNo()); //  проверить кастомеры

        verify(restTemplate, times(1)).getForObject(
                anyString(), eq(FraudResponse.class), any(), eq(customer.getSerNo())
        );
    }

    @Test
    public void getById() {
        Customer serviceResponse = customerController.getCustomerById(testCustomer.getId()).orElse(null);
        assertNotNull(serviceResponse);
        assertEquals(testCustomer.getSerNo(), serviceResponse.getSerNo());
    }

    @Test
    public void deleteById() {
        customerController.deleteCustomerById(testCustomer.getId());
        Optional<Customer> optionalCustomer = customerRepository.findById(testCustomer.getId());
        assertFalse(optionalCustomer.isPresent());
    }

    @Test
    public void getAllCustomers() {
        List<Customer> customer = customerRepository.findAll();
        assertThat(customer).usingRecursiveComparison().isEqualTo(
                Objects.requireNonNull(customerController.getAllCustomers()));
    }

    @Test
    public void updateCustomer() {
        String number = getRandomDigits(111);
        String serNo = testCustomer.getSerNo();

        given(restTemplate.getForObject(
                anyString(),
                eq(FraudResponse.class),
                any(),
                any()
        )).willReturn(new FraudResponse(true));

        assertNotEquals(number, serNo);
        testCustomer.setSerNo(number);
        testCustomer = customerController.updateCustomer(testCustomer);
        int testCustomerId = testCustomer.getId();
        assertNotEquals(serNo, testCustomer.getSerNo());

        Optional<Customer> testCustomer = customerRepository.findById(testCustomerId);
        assertTrue(testCustomer.isPresent());
        assertEquals(number, testCustomer.get().getSerNo());

        verify(restTemplate, times(1)).getForObject(
                anyString(), eq(FraudResponse.class), any(), any()
        );
    }
}